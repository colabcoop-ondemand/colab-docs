# Mobile application frameworks
## [Expo](https://expo.dev/)

Expo is a framework and a platform for universal React applications. It is a set of tools and services built around React Native and native platforms that help you develop, build, deploy, and quickly iterate on iOS, Android, and web apps from the same JavaScript/TypeScript codebase.

* Language: JavaScript
* License: [MIT][MIT]
* [Documentation](https://docs.expo.dev/)

## [React Native](https://reactnative.dev/)

React Native combines the best parts of native development with React, a best-in-class JavaScript library for building user interfaces.

* Language: JavaScript
* License: [MIT][MIT]
* [Documentation](https://reactnative.dev/docs/accessibilityinfo)

[MIT]: https://opensource.org/licenses/MIT