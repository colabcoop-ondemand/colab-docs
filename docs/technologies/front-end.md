# Front-end tools / frameworks

## [Bootstrap](https://getbootstrap.com/)

* Languages / Technolgies: HTML, CSS, JavaScript
* [Documentation](https://getbootstrap.com/docs/5.1/getting-started/introduction/)

Bootstrap is a free and open-source CSS framework directed at responsive, mobile-first front-end web development. It contains CSS- and JavaScript-based design templates for typography, forms, buttons, navigation, and other interface components.

Strengths:
* Easy to rapidly prototype layouts and interfaces
* Widely used
* CDN available

Weaknesses:
* Pre-built components can sometimes be limiting
* Can be overkill for smaller projects, or where a lot of customization needs to occur


## [Chakra UI](https://chakra-ui.com/)

Chakra UI is a simple, modular and accessible component library that gives you the building blocks you need to build your React applications.

* Languages / Technolgies: HTML, CSS, JavaScript
* [Documentation](https://chakra-ui.com/docs/getting-started)

## [Material UI](https://mui.com/)

MUI provides a robust, customizable, and accessible library of foundational and advanced components, enabling you to build your own design system and develop React applications faster.

* Languages / Technolgies: HTML, CSS, JavaScript
* [Documentation](https://mui.com/getting-started/usage/)

## [React](https://reactjs.org/)

A JavaScript library for building user interfaces

* Language: JavaScript
* [Documentation](https://reactjs.org/docs/getting-started.html)


## [Angular](https://angular.io)

* Language: JavaScript
* [angular.io documentation](https://angular.io/docs)

HTML is great for declaring static documents, but it falters when we try to use it for declaring dynamic views in web-applications. AngularJS lets you extend HTML vocabulary for your application. The resulting environment is extraordinarily expressive, readable, and quick to develop.

Angular is an application design framework and development platform for creating efficient and sophisticated single-page apps.

**NOTE**: AngularJS support officially ended as of January 2022.

* [Version Support Status](https://docs.angularjs.org/misc/version-support-status)
* [End of life announcement](https://blog.angular.io/discontinued-long-term-support-for-angularjs-cc066b82e65a)
* [angular.io](https://angular.io/) - the actively supported Angular.

### [Vue.js](https://vuejs.org/)

* Language: JavaScript
* [Documentation](https://vuejs.org/v2/guide/)


[MIT]: https://opensource.org/licenses/MIT