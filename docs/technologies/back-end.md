# Back-end (Application) Frameworks

## [Rails](https://rubyonrails.org/)

Rails is a web application framework using a model–view–controller (MVC) paradigm. It emphasises a "Convention Over Configuration" approach to building a web application, with sensible default structures for a database, a web service, and web pages.

* Licence: [MIT][MIT]
* [Documentation](https://guides.rubyonrails.org/)

## [Laravel](https://laravel.com/)

Laravel is a web application framework with expressive, elegant syntax. A web framework provides a structure and starting point for creating your application, allowing you to focus on creating something amazing while we sweat the details.

Laravel strives to provide an amazing developer experience while providing powerful features such as thorough dependency injection, an expressive database abstraction layer, queues and scheduled jobs, unit and integration testing, and more.

* Language: PHP
* Licence: [MIT][MIT]
* [Documentation](https://laravel.com/docs/)
* Screencasts: [https://laracasts.com/](https://laracasts.com/)


## [Django](https://www.djangoproject.com/)

Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design.

* [Documentation](https://docs.djangoproject.com/en/4.0/)
* Language: Python
* Licence: [BSD 3-clause license](https://github.com/django/django/blob/main/LICENSE)


## [Symfony](https://symfony.com/)

Symfony's strengths:
* reuse generic components
* no need to to develop or redevelop generic features, such as form management
* devs can focus on the application's real challenges
* tools for ease of use and improved productivity
* strong security policies

Probably the most popular PHP framework for web applications. Includes the Symfony Components, a set of decoupled and reusable components. Some of the biggest PHP applications are built on Synfony, including Drupal, phpBB, and eZ Publish.

* Language: PHP
* [Documentation](https://symfony.com/doc/current/index.html)
* Screencasts: [SymfonyCasts](https://symfonycasts.com/)
* Language: PHP
* License: [MIT][MIT]


## [Phoenix](https://www.phoenixframework.org/)

Phoenix is the leading web framework in the Elixir ecosystem. It’s perfect for productively building scalable and reliable web experiences.

* [Overview](https://hexdocs.pm/phoenix/overview.html)
* Language: Elixir
* License: [MIT][MIT]



[MIT]: https://opensource.org/licenses/MIT