Drupal 9 uses composer as dependency manager.

## Naming Convention
Projects(Modules, themes, profiles) must use the package name.
```php
drupal/SUBPROJECT
```
where *SUBPROJECT* is the machine name of the module, theme or profile.

## Components
Drupal projects may also contain custom components (like PHP libraries). As those components are not bound to any
namespace, they are likely to conflict with a given Drupal project, module, theme,

Therefore , Package names must be prefixed with their parent's name and a dash (-),
in the case it will use the drupal/ vendor.
```php
drupal/PARENT-COMPONENT
```
where *PARENT* is the name of the parent package and *COMPONENT* a sufficient name for the component.

## Examples
* Datetime becomes drupal/core-datetime
* Diff becomes drupal/core-diff
