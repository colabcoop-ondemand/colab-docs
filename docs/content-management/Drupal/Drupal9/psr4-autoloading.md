Drupal 8 implements the PSR-4 standard for package-based PHP namespace autoloading by the
PHP Framework Interoperability Group.
Autoloading works for both modules and themes.
In order to write custom modules in D9 , there has to be followed a specific directory structure and notation.
Example sample.module directory structure.
Some basic rules are -

* Each module has a namespace that corresponds to its module name.
```php
Drupal\sample
```

* The module's namespace is mapped to the ./src/ folder in the module directory.
```php
Drupal\sample\ → modules/sample/src/
```

* Anything after the module namespace directly maps to the directory and file structure in the ./src/ folder.
```php
Drupal\sample\Entity\Tomato → modules/sample/src/Entity/Tomato.php
```

* The identical logic applies to PHPUnit tests contained in *./tests/src*
* The modules/sample/src/Tests folder contains the SimpleTest test code.
* The modules/sample/tests folder contains the PHPUnit test code.


More details can be found at [D.o psr4 documentation page](https://www.drupal.org/docs/develop/standards/psr-4-namespaces-and-autoloading-in-drupal-8)
