Drupal9 follows Object oriented paradigm as its architectural paradigm.
All the code written in D9 follow OOPs architecture and practices.

Below are the practices to follow while writing D9 code -

## Declaring Classes
* In Drupal 8, classes will be autoloaded based on the PSR-4 namespacing convention.
* In core, the PSR-4 'tree' starts under *core/lib/* .
* In modules, including contrib, custom and those in core, the PSR-4 'tree' starts under *modulename/src* .

## Indenting and Whitespace
* Leave an empty line between start of class/interface definition and property/method definition
* Leave an empty line between end of property definition and start method definition.
* Leave an empty line between end of method and end of class definition:


```php
class GarfieldTheCat implements FelineInterface {
  // Leave an empty line here.
protected $lasagnaEaten = 0;
  // Leave an empty line here.
  public function meow() {
    return t('Meow!');
    // Leave an empty line here.
  }
```

## Naming conventions
* Classes and interfaces should use UpperCamel naming.
* Methods and class properties should use lowerCamel naming.
* lasses should not use underscores in class names unless absolutely necessary to derive names inherited class names dynamically.
* Names should not include "Drupal".
* Class names should not have "Class" in the name
* Interfaces should always have the suffix "Interface".
* Test classes should always have the suffix "Test.
* Protected or private properties and methods should not use an underscore prefix.

## Use of interfaces
* The use of a separate interface definition from an implementing class is strongly encouraged because it allows more
flexibility in extending code later.
* If there is even a remote possibility of a class being swapped out for another implementation at some point in the future,
split the method definitions off into a formal Interface.
* A class that is intended to be extended must always provide an Interface that other classes can implement rather than
forcing them to extend the base class.

## Visibility
* All methods and properties of classes must specify their visibility: public, protected, or private.

## Instantiation
* Creating classes directly is discouraged
* Instead, use a factory function that creates the appropriate object and returns it. Benefits are -
* It provides a layer of indirection, as the function may be written to return a different object
(with the same interface) in different circumstances as appropriate.
* PHP does not allow class constructors to be chained, but does allow the return value from a function or method to be
chained.

## Chaining
* PHP allows objects returned from functions and methods to be "chained", that is, a method on the returned object
may be called immediately.
```php
$title = db_query("SELECT title FROM {node} WHERE nid = :nid", array(':nid' => 42))->fetchField();
```
* where you have a fluid interface for a class, and the code spans more than one line, the method calls should be
indented with 2 spaces.
```php
$query = db_select('node')
  ->condition('type', 'article')
  ->condition('status', 1)
  ->execute();
```



