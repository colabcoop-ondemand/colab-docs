Drupal 9 uses YAML files for configuration management.

## Introductory notes on the configuration API -
* The file name for a configuration object is equal to the unique name of the configuration, with a '.yml' extension.
* Each configuration file has a specific structure, which is expressed as a YAML-based configuration schema.
* Configuration object can be retrieved  by calling
```php
$config = \Drupal::config('mymodule.foo');
```
and
```php
$value = $config->get('top_level.next_level.another_level');
```
etc. to get the values from the configuration object.


## Filename
* The configuration file name is equal to the unique configuration name with .yml extension.
* The unique configuration name cannot exceed 250 characters.

## Simple configuration
* For simple configuration, the unique configuration name must start with the extension name.
Ex - mymodule.settings or mymodule.features

## Configuration entities
* For configuration entities, the unique configuration name has a prefix, which is equal to (extension).(config_prefix).
* Here, (extension) is the machine name of the module that defines the config entity, or "core" for core entities
* (config_prefix) is defined in the entity annotation, and defaults to the machine name (ID) of the config entity
if it has not been overridden by a config_prefix annotation in the entity class.
* Extension names cannot exceed 50 characters, and config entity config prefixes cannot exceed 32 characters.
* The rest of the unique configuration name for a config entity (which is called the suffix in the rest of this section)
is limited to 150 characters.
* For many configuration entities, the suffix consists solely of the individual machine name of the item.
Ex- the unique configuration name for an image style is image.style.(machine_name_of_style), and for a view
it is views.view.(machine_name_of_view)

## Format (yml)
Config files use YAML syntax and .yml extension.

## Comments
Comments are not typically in config files, but can be made using #.

## Whitespace
Use two spaces to indent in config files. In YAML, the white space has semantic meaning to represent nested structures.


