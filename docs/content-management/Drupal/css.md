CSS is a stylesheet language used to describe the presentation of a HTML document. CSS describes how HTML
elements should be displayed.

## Indenting
* Do not use tabs for indentation.
* Use 2 spaces for each level of indentation.
* Declarations (property/value pairs) should be indented one level relative to their selector.
* Rulesets within a media block or a media query should be indented one level relative to the media statement.
* Comments should be indented the same amount as the declaration or ruleset they describe.
```php
@media print {
  /* This line is indented with 2 spaces, 2 spaces x 1 level of indentation. */
  .example {
    /* This line is indented with 4 spaces, 2 spaces x 2 levels of indentation. */
    padding: 0;
  }
}
```
## Blank Lines
* In general, do NOT separate each ruleset by a blank line.
* If a ruleset has a preceding Doxygen-style or single-line-style comment that describes it,
place a blank line before the comment.

## Line endings
* There MUST NOT be any whitespace (spaces or tabs) at the end of lines. This means blank lines should also not contain
any spaces or tabs.

## File comments
* Each file should start with a comment describing what the file does.

## Multi-line comments
* When describing a ruleset or set of rulesets, any comment that requires 2 or more lines (wrapped to 80 characters)
must follow the Doxygen comment style (also called a “docblock”).

## Single-line comments
* When describing a property or ruleset, any comment that can be written inside the 80 character line length limit can
use a simple CSS comment style.

## Rulesets
* Use one selector per line when a ruleset has a group of selectors separated by commas.
* The opening brace ({) of a ruleset’s declaration block should be on the same line as the selector
(or the same line as the last selector in a group of selectors.)
* Place the closing brace (}) of a ruleset in the same column as the first character in the selector of the ruleset.
* Include one declaration per line in a declaration block.
* Each declaration should be indented one level relative to its selector.

## Properties
* In a declaration, the property name should be immediately followed by a colon, then a single space,
and then the property’s value.
* Include a semi-colon at the end of all declarations, including the last declaration in a declaration block.
* When hex values are used for colors, use lowercase and, if possible, the shorthand syntax, e.g. #aaa
* For property values that require quotes, use double quotes instead of single quotes,
* If a property does not require quotes (e.g. url(), do not add them.
* Use rem units preceded by px units for a safe fallback, unless it creates an undesired effect
* Use lower case function names, correct: color: rgba(0, 0, 0, 0.8); incorrect: color: RGBA(0, 0, 0, 0.8);
* Do not use spaces around the parentheses in a function, e.g. color: rgba(0, 0, 0, 0.8);

## Media Queries


