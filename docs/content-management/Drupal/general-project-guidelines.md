Some general Drupal project guidelines to be followed are -
1. Follow the drupal coding standards and paradigm as per the drupal version being used.
2. Configure the code sniffer in the project to flag code which violates the coding practices.
3. Configure pre-commit hook which should check for any violations raised by code sniffer and therefore prevent the code
from being committed.
4. Configure pre-commit hook to check for JIRA project key and JIRA ticket number as part of commit message in order to
tie commit with a specific JIRA ticket number and functionality for backtracking purpose.
5. Project should include *tests*. Tests should check for Frontend rendering markup functionality , custom module functionality,
and Backend architecture (entities and its corresponding fields.)
6. Configure pre-commit  hook to also run the written tests. There should be a flag to skip certain tests in order to pass
the *WIP functionality*.
7. There should be template to be followed when raising a Pull request. Template/Format code should be part of every
pull-request description. Sample template below
```php
JIRA ticket number -
Technical functionality added -
Steps to test -
```
8. Once Pull request is raised - JIRA ticket should be updated with details like - PR link , steps to test for QA team
and passed to next team member for review , merge and deployment.
9. There should be proper JIRA workflow status defining the state/ownership of the ticket.
