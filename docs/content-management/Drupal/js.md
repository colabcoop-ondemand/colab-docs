JavaScript is a text-based programming language used both on the client-side and server-side that allows you to make
web pages interactive.

## Indenting
* All code MUST indent using two (2) space characters,
* All code MUST NOT indent using tab characters,
* All code MUST NOT end with trailing whitespace.

## Semicolons
* JavaScript allows optional "semi-colon insertion". Drupal standards do not.
* Return values MUST start on the same line as the return keyword.
* Anonymous functions assigned to a variable MUST be followed by a semi-colon.
```php
Drupal.behaviors.tableSelect = function (context) {
  // Statements...
};
```
* do/while control structures MUST be followed by a semi-colon
```php
do {
  // Statements...
} while (condition);
```

## File-closure
* All JavaScript code MUST be declared inside a closure wrapping the whole file.
 ```php
* /**
   * @file
   */
  (() => {

// All the JavaScript for this file.

})();
```

## CamelCasing
* For variables that are not constants or constructors, multi-word variables and functions SHOULD be lowerCamelCased.

## Variables and Arrays
* All variables MUST be declared with var before they are used and SHOULD be declared only once.

## Constants
* Pre-defined constants SHOULD be all-uppercase and words separated by underscores: UPPER_UNDERSCORED.

## Arrays
* Arrays SHOULD be formatted with one space separating each element and the assignment operator.
```php
var someArray = ['hello', 'world'];
```

## Typeof
```php
if (typeof myVariable === 'string') {
// ...
}
```

## Functions names , declaration and function calls
* Function names SHOULD begin with the name of the module or theme declaring the function.
* The function keyword MUST be followed by one space.
* Named functions MUST NOT have a space between the function name and the following left parenthesis.
* Optional arguments (using default values) SHOULD be defined at the end of the function signature.
* Every function SHOULD attempt to return a meaningful value.
* Functions SHOULD be called with no spaces between the function name, the opening parenthesis, and the first parameter.
There SHOULD be one space between commas and each parameter, and there SHOULD NOT be a space between the last parameter,
the closing parenthesis, and the semicolon.

## String Concatenation
* Expressions SHOULD be separated with one space before and after the + operator to improve readability.
* The concatenating assignment operator (+=) SHOULD be separated with one space on each side as with the assignment
operator.

## Control Structures - if, for, while, switch
* Control statements MUST have one space between the control keyword and opening parenthesis, to distinguish them from function calls.
* Control structures MUST always use curly braces.

## Operators
* Strict equality MUST be used in comparisons (=== or !==).
* You SHOULD NOT use the comma operator, with the exception of the control part in for statements

## Use literal expressions
* Code SHOULD use literal expressions instead of the new operator.
```php
Use [] instead of new Array()
Use {} instead of new Object()
```
* The with statement MUST NOT be used, since it is not possible to use *with* with enabled strict mode.
* To prevent unreachable code, a *return*, *break*, *continue*, or *throw* statement SHOULD be followed by a *}* or
*case* or *default*.
* *eval()* SHOULD NOT be used.
* All output to the browser that has been provided by a user SHOULD be escaped through Drupal.checkPlain() first in
order to prevent XSS attacks.
* When adding new HTML elements to the DOM, you SHOULD NOT use document.createElement() . Instead,
for cross-browser compatibility reasons and also in an effort to reduce file size, you SHOULD use the jQuery equivalent.
```php
this.popup = $('<div id="autocomplete"></div>')[0];
```

## ESLint - Tool to detect js syntax errors and consistency and minify js.
ESLint configuration files ship with Drupal. Drupal core code will be automatically detected and used by ESLint when
it is invoked from within the code base.
Custom js code should be checked with ESLint.

## jQuery coding standards
jQuery standards can be found at https://www.drupal.org/docs/develop/standards/javascript/jquery-coding-standards

## Resources
* https://www.drupal.org/docs/develop/standards/javascript/eslint-settings
* https://www.drupal.org/docs/develop/standards/javascript/jquery-coding-standards





