# Drupal

**Drupal** is a free and open-source web content management system (CMS) written in PHP and distributed under the GNU
General Public License.

As of March 2021, the Drupal community comprised more than 1.39 million members, including 121,000 users actively
contributing,resulting in more than 46,800 free modules that extend and customize Drupal functionality,over 2,900
free themes that change the look and feel of Drupal,and at least 1,300 free distributions that allow users to quickly
and easily set up a complex, use-specific Drupal in fewer steps.

The standard release of Drupal, known as Drupal core, contains basic features common to content-management systems.
These include user account registration and maintenance, menu management, RSS feeds, taxonomy, page layout customization,
and system administration. The Drupal core installation can serve as a simple website, a single- or multi-user blog,
an Internet forum, or a community website providing for user-generated content.

##Resources
Coding standards: https://www.drupal.org/docs/develop/standards/coding-standards

Note - Drupal coding standards are version-independent and "always-current". All new code should follow the current standards, regardless of (core) version.

## Active/Supported Drupal Version
**Drupal7**

**Drupal9**

## Section Index for Coding standards

###Formatting
###API documentation
###CSS
###JavaScript coding standards
###Markdown
###Namespaces
###Object-oriented code
###PHP Exceptions
###PSR-4 namespaces and autoloading
###SQL coding convention
###Avoid "SELECT * FROM ..."
###Twig coding standards
###Drupal Markup Style Guide
###Configuration file coding standards
###Composer package naming conventions
###Drupal SimpleTest coding standards
###Twig coding standards

