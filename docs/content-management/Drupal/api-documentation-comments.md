API module parses documentation and code in PHP files, and it expects documentation to be in a specific format.
Writing format was originally based on Doxygen, but it has evolved into something that has its own set of tags
and a lot of Drupal-specific functionality.
The API module parses documentation that is in special documentation blocks (known as "docblocks") .

Below are the rules for writing comments for the php files.

* The special documentation tags described are only recognized within special PHP comment blocks that
  start with /**. These are known  as docblocks.
* In-code comment lines starting with // and comment blocks starting with /* are not recognized as docblocks.
* Docblocks normally have * at the beginning of each line, and the API module strips these out when formatting
the documentation.
* To make a paragraph break in a docblock, leave a blank line. Some tags also trigger paragraph breaks: @param,
 @return, @see, @var. The API module does not support blank lines within a single tag's documentation .
* The first paragraph of a docblock is known as the summary.
* To document a function, class, etc., the docblock must appear directly before the item being documented, with no
blank line in between.
* Every function, constant, class, interface, class member (function, property, constant), and file must be documented,
even private class members.
* All summaries (first lines of docblocks) must be under 80 characters, start with a capital letter,
and end with a period (.).
* When implementing a hook, use a short summary of the form "Implements hook_menu()."
* The @var tag can document variables in addition to class properties.
* Hook implementation -
  /**
  * Implements hook_help().
  */
* Update functions (implementations of hook_update_N()) -
```php
  /**
  * Add several fields to the {node_revision} table.
  *
  * Longer description can go here, if necessary.
  */
 ```
  * Callbacks for built-in PHP functions -
    ```php
    /**
    * [standard first line]
    *
    * Callback for uasort() within system_themes_page().
    ```
  * Form-generating functions -
 ```php
  /**
  * Form constructor for the user login form.
  *
  * @param string $message
  * The message to display.
  *
  * @see user_login_form_validate()
  * @see user_login_form_submit()
  *
  * @ingroup forms
  */
  function user_login_form($form, &$form_state, $message = '') {
  …
  }
```
* @file: Documenting files -
```php
/**
 * @file
 * The theme system, which controls the output of Drupal.
 *
 * The theme system allows for nearly all output of the Drupal system to be
 * customized by user themes.
 */
```

## Resources
https://www.drupal.org/docs/develop/standards/api-documentation-and-comment-standards
Complete list of doc standards and keywords and usage can be found on the above link.

