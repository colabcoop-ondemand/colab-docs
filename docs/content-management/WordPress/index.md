# WordPress

[WordPress](https://wordpress.org) is a content management system written in PHP and paired with a MySQL or MariaDB database.

WordPress was originally created as a blog-publishing system but has evolved to support other web content types including more traditional mailing lists and forums, media galleries, membership sites, learning management systems (LMS) and online stores. One of the most popular content management system solutions in use, 

WordPress is the world's leading content management system, used by 42.8% of the top 10 million websites as of October 2021.

## Section Index

* [Development Setup](dev-setup.md)
* [Coding Standards](coding-standards.md)
* [Plugins](plugins.md) - Info on creating plugins to extend WordPress functionality.
* [Themes](themes.md) - Info on creating themes to customize the look and feel of a WordPress site.
* [Blocks](blocks.md) - Info on creating custom blocks (components) for the block editor.
* [WordPress Resources](resources.md)
* [WordPress Hosting](hosting.md)