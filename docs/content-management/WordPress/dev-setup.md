# WordPress Development Setup

## Project scaffolding

There are two basic ways you can set up WordPress project:

### Use WordPress as-is

The first and most obvious is to use the WordPress codebase as-is. Plugins and theme updates are handled by WordPress itself. You can choose whether or not to include plugins and themes in git, or only include some (i.e. custom plugins or themes). This is most suitable for smaller projects, or ones hosted somewhere without access to Composer and/or build tooling (i.e. shared hosting, WPEngine, etc).

### Manage WordPress with Composer

Most of the PHP community has moved to using Composer to manage dependencies. To use a Composer-based workflow with WordPress, you can use the [Bedrock](https://roots.io/bedrock/) stack. 

[Bedrock](https://roots.io/bedrock/) is a modern WordPress stack that helps you get started with the best development tools and project structure. Bedrock brings the following advantages over the *as-is* WordPress setup:

* Better folder structure
* Dependency management with [Composer](https://getcomposer.org)
* Easy WordPress configuration with environment specific files
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)
* Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))

[More info about using Composer with WordPress](composer-bedrock.md).

## Local development environments

Here are some useful methods of setting up a local development environment for WordPress:

### Lando

[Lando](https://lando.dev/) is a free, open source, cross-platform, local development environment and DevOps tool built on Docker container technology.

Designed to work with most major languages, frameworks and services, Lando provides an easy way for developers of all skill levels to specify simple or complex requirements for their projects, and then quickly get to work on them.

Advantages:

* Quickly specify and painlessly spin up the services and tooling needed to develop all their projects.
* Ship these local development dependencies in a per-project, lives-in-git config file
* Automate complex build steps, testing setups, deployments or other repeated-more-than-once workflows
* Avoids the complexity of directly using docker or docker-compose.
* Can support numerous technologies, applications and frameworks
* Lando offers a [configurable recipe](https://docs.lando.dev/config/wordpress.html) for developing WordPress apps.

[Lando Getting Started guide](https://docs.lando.dev/basics/)

### Local

[Local](https://localwp.com/) is a local development tool specifically designed for WordPress.

Advantages:

* Integration with Flywheel and WP Engine hosting, including two-way sync (push and pull).
* Automatic WordPress installation including virtual host and SSL config on your local machine.
* Advanced development features - root SSH access, WP-CLI, and the ability to hot-swap PHP environments.
* Graphical User Interface for management of sites.

[Local Docs](https://localwp.com/help-docs/)


## WordPress specific developer tools

### [WP-CLI](https://wp-cli.org/)

WP-CLI is the command-line interface for WordPress. You can update plugins, configure multisite installations and much more, without using a web browser.

Quick links: [Using](https://wp-cli.org/#using) &#124; [Installing](https://wp-cli.org/#installing) &#124; [Support](https://wp-cli.org/#support) &#124; [Extending](https://wp-cli.org/#extending) &#124; [Contributing](https://wp-cli.org/#contributing) &#124; [Credits](https://wp-cli.org/#credits)
