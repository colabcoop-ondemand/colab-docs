# Blocks

The WordPress block editor (codename **Gutenberg**) is a new publishing experience introduced in WordPress 5.0 in December 2018. The block editor is built primarily in JavaScript using the [React](https://reactjs.org/) library.

The block editor replaces the previous rich text editor based on [TinyMCE](https://www.tiny.cloud/tinymce/), which is now referred to as the *classic* editor.

The block editor experience is somewhere in between a page builder and component based design. It is built on the idea of using “blocks” to write and design posts and pages. Every element on a post or page has its own block — images, headings, galleries, lists, etc. are all blocks.

## Using the block editor

[OSTraining](https://www.ostraining.com/) has posted an excellent video tutorial series [How to Use WordPress 5 and the Gutenberg Editor](https://youtube.com/playlist?list=PLtaXuX0nEZk8A3nkCkuSFbB1QBptvUdAh) on YouTube.

## Extend WordPress with Additional Blocks

In addition to the blocks available in WordPress core, there is a growing ecosystem of third-party plugins (free and paid) that add additional custom blocks. Many of the most popular plugins in the WordPress directory have also added blocks to fully integrate their functionality with the Block editor.

There is a [block-based plugins section](https://wordpress.org/plugins/browse/blocks/) in the official plugin directory that lists plugins that provide blocks.

Here are some of the most popular plugins that provide additional block libraries:

- [CoBlocks](https://wordpress.org/plugins/coblocks/)
- [Genesis Blocks](https://wordpress.org/plugins/genesis-blocks/) - formerly known as Atomic Blocks, acquired by StudioPress, who develop the Genesis framework themes.
- [Stackable](https://wordpress.org/plugins/stackable-ultimate-gutenberg-blocks/)
- [Kadence Blocks](https://wordpress.org/plugins/kadence-blocks/)

A lot of these block library plugins have very similar offerings. It is probably best to pick just one plugin, that best fits the need of the project - otherwise you risk having multiple blocks that are essentially the same, with inconsistency in the site as editors add multiple different blocks to pages to provide the same type of component, each with its own styling and other subtle differences.

## Gutenberg Roadmap

There are four phases of Gutenberg which you can see on the [official WordPress roadmap](https://wordpress.org/about/roadmap/).

1.  Easier Editing — Already available in WordPress since 5.0, with ongoing improvements.
2.  Customization — Full Site editing, Block Patterns, Block Directory, Block based themes.
3.  Collaboration — A more intuitive way to co-author content
4.  Multi-lingual — Core implementation for Multi-lingual sites

Read more about the [Gutenberg, Block Editor and WordPress Roadmap](https://gutenbergtimes.com/mullenweg-on-gutenberg-roll-out-plan/) at the Gutenberg Times.


## Full Site Editing

The block editor editor for building posts and pages was the first phase of the Gutenberg initiative. Lots of work is being done on the second step in the Gutenberg roadmap.

Core devs are working towards being to manage the whole site using blocks. This will include things like menus, the aforementioned widgets, page layouts, etc. This initiative is known as **full-site editing**.

WordPress 5.8 most notably introduced a [block-based editor for Widgets](https://make.wordpress.org/core/2021/06/29/block-based-widgets-editor-in-wordpress-5-8/), and a [Query Loop](https://wordpress.org/support/article/query-loop-block/) block for adding dynamic lists of posts, similar to using the `WP_Query` class or custom loops in traditional PHP templates.

The vision of Full Site Editing is to provide a collection of features that bring the familiar experience and extendability of blocks to all parts of your site rather than just post and pages. 

*   Site Editor: the cohesive experience that allows you to directly edit and navigate between various templates, template parts, styling options, and more.
*   Template Editing: a scaled down direct editing experience allowing you to edit/change/create the template a post/page uses.
*   Block Theme: work to allow for a theme that’s built using templates composed using blocks that works with full site editing. More below.
*   Styling: the feature that enables styling modifications across three levels (local blocks, theme defaults, and global modifications).
*   Theme Blocks: new blocks that accomplish everything possible in traditional templates using template tags (ex: Post Author Block).
*   Browsing: the feature that unlocks the ability to navigate between various entities in the site editing experience including templates, pages, etc.
*   Navigation Block: a new block that allows you to edit a site’s navigation menu, both in terms of structure and design.
*   Query Block: a new block that replicates the classic [WP\_Query](https://developer.wordpress.org/reference/classes/wp_query/) and allows for further customization with additional functionality.

## Block Themes

A block theme is a WordPress theme with templates entirely composed of blocks so that in addition to the post content of the different post types (pages, posts, …), the block editor can also be used to edit all areas of the site: headers, footers, sidebars, etc.

Initial Block theme support is slated for release in WordPress 5.9, in late January 2022. [WPBeginner has published an article](https://www.wpbeginner.com/news/whats-coming-in-wordpress-5-9-features-and-screenshots/) on what to expect.

If you are using the Gutenberg plugin you can run, test, and develop block themes. Block themes are themes built using templates composed using blocks. See [block theme overview](https://developer.wordpress.org/block-editor/how-to-guides/themes/block-theme-overview/) for additional details.

## Block Development

### Native block development with JavaScript

The best place to start for block development is the [Create a Block Tutorial](https://developer.wordpress.org/block-editor/getting-started/create-block/).

The create a block tutorial breaks down to the following sections:

1.  [WordPress Plugin](https://developer.wordpress.org/block-editor/getting-started/create-block/wp-plugin/)
2.  [Anatomy of a Gutenberg Block](https://developer.wordpress.org/block-editor/getting-started/create-block/block-anatomy/)
3.  [Block Attributes](https://developer.wordpress.org/block-editor/getting-started/create-block/attributes/)
4.  [Code Implementation](https://developer.wordpress.org/block-editor/getting-started/create-block/block-code/)
5.  [Authoring Experience](https://developer.wordpress.org/block-editor/getting-started/create-block/author-experience/)
6.  [Finishing Touches](https://developer.wordpress.org/block-editor/getting-started/create-block/finishing/)
7.  [Share your Block with the World](https://developer.wordpress.org/block-editor/getting-started/create-block/submitting-to-block-directory/)

### PHP based block development

In response to the steep learning curve around advanced JavaScript and React, several tools were created by the WordPress ecosystem to aid with rapid development tools, often based in PHP, that enable you to create blocks without extensive knowledge of advanced JavaScript or React.

Some of the most popular block development tools include:

* [Advanced Custom Fields](https://www.advancedcustomfields.com/resources/blocks/) - Included in ACF PRO is a powerful PHP-based framework for developing custom block types. ACF blocks are highly customisable and powerfully dynamic. They integrate deeply with custom fields allowing PHP developers to create bespoke solutions inline with WordPress theme development.
* [Carbon Fields](https://docs.carbonfields.net/learn/containers/gutenberg-blocks.html) - uses a PHP class instance to initiate blocks and assign them fields and configuration in code.
* [Block Lab](https://getblocklab.com/) - A WordPress plugin providing an admin interface and a simple templating system for building custom Gutenberg blocks.

These tools shorten the learning curve when it comes to block development, but they do tend to be slower, and do not always fully fit in with the look and feel of the native blocks.

To provide the best end-user experience, it is reccomended to use native JS-based blocks as much as possible.

## Additional Resources

* [Block Editor](https://developer.wordpress.org/block-editor/) development - complete reference to creating and customizing blocks and the block editor (codename **Gutenberg**)
* [Bill Erickson](https://www.billerickson.net/category/gutenberg-block-editor/) has a section on his blog all about block development.
* [Gutenberg Times](https://gutenbergtimes.com/) - blog curating News and Community Voices about the WordPress Block Editor, codenamed Gutenberg.
* [Get Started With Gutenberg Development](https://gutenberghub.com/gutenberg-developer-guide/) by Gutenberg Hub.
* [WordPress Gutenberg Developer’s Guide](https://awhitepixel.com/guides/wordpress-gutenberg-developers-guide/) by A White Pixel.
* [Gutenberg, Block Editor and WordPress Roadmap](https://gutenbergtimes.com/mullenweg-on-gutenberg-roll-out-plan/) by the Gutenberg Times
