# Developer Resources

# Official WordPress Developer Resources

The [WordPress Developer](https://developer.wordpress.org/) site is the official resource for WordPress development.

* [Code Reference](https://developer.wordpress.org/reference/) - documentation for the codebase, including Functions, Hooks, Classes, Methods.
* [WordPress Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/) - the official coding standards set for WordPress core development.
    *   [Accessibility Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/accessibility/)
    *   [CSS Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/css/)
    *   [HTML Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/html/)
    *   [JavaScript Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/javascript/)
    *   [PHP Coding Standards](https://developer.wordpress.org/coding-standards/wordpress-coding-standards/php/)
    *   [Inline Documentation Standards](https://developer.wordpress.org/coding-standards/inline-documentation-standards/)
    *   [JavaScript Documentation Standards](https://developer.wordpress.org/coding-standards/inline-documentation-standards/javascript/)
    *   [PHP Documentation Standards](https://developer.wordpress.org/coding-standards/inline-documentation-standards/php/)
* [Block Editor](https://developer.wordpress.org/block-editor/) - complete reference to creating and customizing blocks and the block editor (codename **Gutenberg**)
* [Common APIs](https://developer.wordpress.org/apis/) - documentation on all APIs present within the WordPress software as well as APIs available from the WordPress.org ecosystem.
    *   [Responsive Images](https://developer.wordpress.org/apis/handbook/responsive-images/)
    *   [Dashboard widgets API](https://developer.wordpress.org/apis/handbook/dashboard-widgets/)
    *   [Database API](https://developer.wordpress.org/apis/handbook/database/)
    *   [Internationalization](https://developer.wordpress.org/apis/handbook/internationalization/)
    *   [Filesystem](https://developer.wordpress.org/apis/handbook/filesystem/)
    *   [Global Variables](https://developer.wordpress.org/apis/handbook/global-variables/)
    *   [Metadata](https://developer.wordpress.org/apis/handbook/metadata/)
    *   [Options](https://developer.wordpress.org/apis/handbook/options/)
    *   [Plugins](https://developer.wordpress.org/apis/handbook/plugins/)
    *   [Making HTTP requests](https://developer.wordpress.org/apis/handbook/making-http-requests/)
    *   [Quicktags](https://developer.wordpress.org/apis/handbook/quicktags/)
    *   [REST](https://developer.wordpress.org/apis/handbook/rest/)
    *   [Rewrite](https://developer.wordpress.org/apis/handbook/rewrite/)
    *   [Settings](https://developer.wordpress.org/apis/handbook/settings/)
    *   [Shortcode](https://developer.wordpress.org/apis/handbook/shortcode/)
    *   [Site Health](https://developer.wordpress.org/apis/handbook/site-health/)
    *   [Theme](https://developer.wordpress.org/apis/handbook/theme/)
    *   [Transients](https://developer.wordpress.org/apis/handbook/transients/)
    *   [XML-RPC](https://developer.wordpress.org/apis/handbook/xml-rpc/)
* [Themes](https://developer.wordpress.org/themes/) - reference on how to develop themes for WordPress
* [Plugins](https://developer.wordpress.org/plugins/) - plugin authoring reference. Extend WordPress functionality.
* [REST API](https://developer.wordpress.org/rest-api/)
* [WP-CLI](https://developer.wordpress.org/cli/commands/) - command-line interface for WordPress.
