# WordPress hosting

## Requirements

WordPress has pretty minimal hosting requirements:

- [PHP](https://www.php.net/) version 7.4 or greater.
- [MySQL](https://www.mysql.com/) version 5.7 or greater OR [MariaDB](https://mariadb.org/) version 10.2 or greater.
- HTTPS support
- A web server that supports PHP and MySQL - [Apache](https://httpd.apache.org/) or [Nginx](https://nginx.org/) are reccomended.

For detailed PHP extension recommendations, see the [WordPress Hosting Handbook](https://make.wordpress.org/hosting/handbook/server-environment/).

## Recommended Hosting providers for WordPress