# Using Composer with WordPress

## WordPress Packagist

The official WordPress plugins and themes directories do not support Composer. To enable management of plugins and themes via Composer, we can use [WordPress Packagist](), a third party repository that mirrors the WordPress [plugin](https://plugins.svn.wordpress.org/) and [theme](https://themes.svn.wordpress.org/) directories as a [Composer](https://getcomposer.org/) repository.

### How do I use it?

1.  Add the WordPress Packagist repository to the *repositories* section of your `composer.json`
2.  Add the desired plugins and themes to your requirements using `wpackagist-plugin` or `wpackagist-theme` as the vendor name.

Example `composer.json`:

```json
{
    "name": "acme/brilliant-wordpress-site",
    "description": "My brilliant WordPress site",
    "repositories": [{
        "type": "composer",
        "url": "https://wpackagist.org",
        "only": [
            "wpackagist-plugin/*",
            "wpackagist-theme/*"
        ]
    }],
    "require": {
        "aws/aws-sdk-php": "*",
        "wpackagist-plugin/akismet": "dev-trunk",
        "wpackagist-plugin/wordpress-seo": ">=7.0.2",
        "wpackagist-theme/hueman": "*"
    },
    "autoload": {
        "psr-0": {
            "Acme": "src/"
        }
    },
    "extra": {
        "installer-paths": {
            "wp-content/mu-plugins/{$name}/": [
                "wpackagist-plugin/akismet"
            ],
            "wp-content/plugins/{$name}/": [
                "type:wordpress-plugin"
            ]
        }
    }
}
```

This example `composer.json` file adds the WordPress Packagist repository and includes the latest version of Akismet (installed as a must-use plugin), at least version 3.9 of Captcha, and the latest Hueman theme along with the Amazon Web Services SDK from the main [Packagist](https://packagist.org/) repository.

## Bedrock

[Bedrock](https://roots.io/bedrock/) is a modern WordPress stack that helps you get started with the best development tools and project structure.

Much of the philosophy behind Bedrock is inspired by the [Twelve-Factor App](http://12factor.net/) methodology including the [WordPress specific version](https://roots.io/twelve-factor-wordpress/).

### Bedrock Features

* Better folder structure
* Dependency management with [Composer](https://getcomposer.org)
* Easy WordPress configuration with environment specific files
* Environment variables with [Dotenv](https://github.com/vlucas/phpdotenv)
* Autoloader for mu-plugins (use regular plugins as mu-plugins)
* Enhanced security (separated web root and secure passwords with [wp-password-bcrypt](https://github.com/roots/wp-password-bcrypt))

### Bedrock Requirements

- PHP >= 7.4
- Composer ([Installation](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos))

### Getting Started

1. Create a new project:
    ```bash
    $ composer create-project roots/bedrock
    ```
2. Update environment variables in the `.env` file (ignore this file in git):
  - Database variables
    - `DB_NAME` - Database name
    - `DB_USER` - Database user
    - `DB_PASSWORD` - Database password
    - `DB_HOST` - Database host
    - Optionally, you can define `DATABASE_URL` for using a DSN instead of using the variables above (e.g. `mysql://user:password@127.0.0.1:3306/db_name`)
  - `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  - `WP_HOME` - Full URL to WordPress home (https://example.com)
  - `WP_SITEURL` - Full URL to WordPress including subdirectory (https://example.com/wp)
  - `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`
    - Generate with [wp-cli-dotenv-command](https://github.com/aaemnnosttv/wp-cli-dotenv-command)
    - Generate with [our WordPress salts generator](https://roots.io/salts.html)
3. Add theme(s) in `web/app/themes/` as you would for a normal WordPress site
4. Set the document root on your webserver to Bedrock's `web` folder: `/path/to/site/web/`
5. Access WordPress admin at `https://example.com/wp/wp-admin/`

### Multisite

Bedrock is multisite network compatible, but needs the [roots/multisite-url-fixer](https://github.com/roots/multisite-url-fixer) mu-plugin on subdomain installs to make sure admin URLs function properly. This plugin is not _needed_ on subdirectory installs but will work well with them. From your Bedrock directory:

```bash
$ composer require roots/multisite-url-fixer
```

### Bedrock Documentation

The full Bedrock documentation is available at [https://roots.io/bedrock/docs/](https://roots.io/bedrock/docs/).

:material-github: [Bedrock GitHub Repository](https://github.com/roots/bedrock)
