# CoLab Kube

Kube is CoLab's custom hosting and devops platform, built on [Kubernetes](https://kubernetes.io/docs/home/) clusters. It enables consistent deployments and environments, that are scalable and repeatable.

## New Kube platform

The latest iteration of the CoLab Kube was rolled out in May 2023. CoLab's devops team is revamped the Kube platform with the aim of simplifying and improving the developer experience.

The new Kube platform moves from having separate clusters for each environment (development, staging, production), to one cluster that will house all of your environments.

We will be leveraging a private GitLab instance to push our code to for deployments, that will include to continuous integration and deployment tools. You will add this instance as a second git remote on your projects, similar to how one would work with projects hosted on Pantheon or Platform.sh.

We'll are also introducing a new [CLI tool](#cli) for interacting with the Kube platform, which will include commands for all the common tasks that  you would need for connecting a codebase to Kube and managing the Kube environments for your project.

The new Kube platform will have several visualization and metrics dashboards to monitor performance and discover potential issues with your project on Kube.

## 1Password (credential / password manager)

CoLab uses the [1Password](https://1password.com/) password manager to store credentials and share them among teams. It is a multiplatform app that works on MacOS, Windows, Linux, iOS and Android. It also has plugins for most major web browsers.

If you don't have 1Password installed on your system, please [download](https://1password.com/downloads/) and install it.

You should also have received an email at your CoLab email address inviting you to join CoLab Cooperative on 1Password. If you haven't yet accepted, or haven't received an invite, please reach out to the Delivery circle.

Once you have accepted the invitation, you will be able to log in to 1Password with your CoLab email and will be able to access password that have been shared with you, as well as having your own private credential storage.

## CoLab Vault

You will receive an email from DevOps with a shared credential for the [Vault](https://vault.kube.v1.colab.coop/) - where credentials and tokens are stored securely, and then can be passed on as environment variables in your server environment. Please accept this shared credential, and save it in your 1Password app.

The first thing you will do is to log in to the vault using the new credentials you saved in 1Password.

Once logged in you will be brought to the **Secrets** page. 

![Vault - Secrets page](/servers/kube/secrets-engines.png)

You will see one parent entry in the format of user-**myname**. Click on that user name to see all the available services you have credentials for.

![Vault - list of available services](/servers/kube/secrets-engines.png)

If you click on a service entry, it will bring you to a list of your secrets saved for this service.

![Vault secrets - dashboard](/servers/kube/secrets-dashboard.png)

You can copy the secret by clicking the clipboard icon, and see it in plain text by clicking the eye icon.

Please test the logins for each service, and take a quick look around to familiarize yourself with each service.

A partial list of services you may have access to:

* [Kubernetes Dashboard](https://dash.kube.v1.colab.coop/) - the Dashboard is a web-based Kubernetes user interface. The Kubernetes Dashboard gives an overview of applications running on your cluster, and provides information on the state of Kubernetes resources in your cluster and on any errors that may have occurred.
* [GitLab](https://git.kube.v1.colab.coop/) - code repository hosting along with automated deployment workflows. We'll be pushing code to this gitlab instance to trigger deployments to Kube.
* [MinIO](https://minio.kube.v1.colab.coop/) - high-performance, S3 compatible object storage, native to Kubernetes.
* [Grafana](https://grafana.kube.v1.colab.coop/) - metrics visualization dashboards.
* [Kibana](https://kibana.kube.v1.colab.coop/) - live tailing of application / container logs
* [pgAdmin](https://pga.kube.v1.colab.coop/) - a web-based management tool for Postgres databases.
* [phpMyAdmin](https://pma.kube.v1.colab.coop/) - a web-based administration tool for MySQL and MariaDB databases.


## Install Kube dependencies

To work with Kube, you will need to have the following dependencies installed on your host system:

* [kubectl](https://kubernetes.io/docs/tasks/tools/#kubectl)
* [git](https://git-scm.com/downloads)
* [jq](https://stedolan.github.io/jq/download/)
* [yq](https://github.com/mikefarah/yq/#install)
* [go](https://go.dev/doc/install)
* [docker](https://docs.docker.com/engine/install/)
* Terraform
    * [tfswitch](https://tfswitch.warrensbox.com/Install/)
    * [terraform-docs](https://github.com/terraform-docs/terraform-docs)
    * [terraform-nodeping](https://github.com/softkraftco/terraform-nodeping)
* Helm
    * [helm](https://helm.sh/docs/intro/install/)
    * helm-push

## Command-line interface (CLI)

The Colab Kube command-line interface (CLI) tool aims to of provide commands for all of the major tasks that a developer will need to interact with the Kube platform. A basic set of commands was be introduced at launch, with more added over time.

The CLI tool will help with tasks such as:

* downloading and installing your private SSH keys for use with Kube
* syncing between Kube environments
    * sync databases between environments
    * sync files that aren't manged with git, such as user-uploaded images etc.
    * sync between your local development environment and Kube.
* interactions with specific tools in the Kube platform

The CLI tool will have built in help, by by adding a `--help` flag to each command (you've probably seen this in other CLI tools such as Drush, WP-CLI or Composer)

### Install Kube CLI

Make sure you have a `$HOME/.local/bin` folder in your PATH.

If you don't have it, add the following line to your `.bash_profile` file:

```bash
export PATH=$PATH:$HOME/.local/bin
```

Then load the bash_profile:

```
source ~/.bash_profile
```

Now run the following command from your terminal:

```
curl -s https://raw.githubusercontent.com/colab-coop/kube/main/install | bash 
```

This will install the Kube CLI script.

### Dev setup

Setup your local development environment to work with Kube. The following will download your SSH keypair for pushing to the Kube GitLab instance, as well as interacting with the cluster.

```
kube dev setup \
    --alias colab-kube-production-v1 \
    --vault-url https://vault.kube.v1.colab.coop/ \
    --vault-username 'USERNAME' \
    --vault-password 'VAULT_PASSWORD'
```

replace [USERNAME] and [VAULT_PASSWORD] with your vault credentials

## Updated deployment workflow with GitLab instance

As part of the Kube updates, deployments will be triggered via git push to specific branches on a private GitLab instance. This means that the git workflow will change slightly. In addition to your main remote repository on Bitbucket, you will need to add a second remote for your project in git. This may be familiar to those of you working on projects hosted on Pantheon or Platform.sh, as we often use their remotes for deployment only.

To see the remotes you currently have in your code, use:

```
git remote -v
```

You’ll probably see a remote called ‘origin’ by default

```
origin       git@bitbucket.org:colabcoop-ondemand/myproject.git (fetch)
origin       git@bitbucket.org:colabcoop-ondemand/myproject.git (push)
```

To add a second remote, simply add it with a different name (i.e. ‘gitlab’):


```
git remote add gitlab git@git.kube.v1.colab.coop/myproject.git
```


Now when you list your remotes, you will see two:

```
origin       git@bitbucket.org:colabcoop-ondemand/myproject.git (fetch)
origin       git@bitbucket.org:colabcoop-ondemand/myproject.git (push)
gitlab       git@git.kube.v1.colab.coop/myproject.git (fetch)
gitlab       git@git.kube.v1.colab.coop/myproject.git (push)
```

Once you have the second remote, you will need to pull from the gitlab remote to get the Kube config - the branch will be the environment name (i.e. development, staging, production, etc.). The following flags will need to be included:


```
git pull gitlab <branch> --allow-unrelated-histories --no-rebase
```

Now when you want to push to a Kube environment, simply specify the gitlab remote as the destination:

```
git push gitlab <branch>
```

## Resources

* [Kuvernetes Documentation](https://kubernetes.io/docs/home/)
* [Kubernetes Dashboard](https://kubernetes.io/docs/tasks/access-application-cluster/web-ui-dashboard/)
* [MinIO](https://min.io/)
* [Grafana](https://grafana.com/)
* [Kibana](https://www.elastic.co/kibana/)
* [pgAdmin Documentation](https://www.pgadmin.org/docs/pgadmin4/latest/index.html)
* [phpMyAdmin Documentation](https://www.phpmyadmin.net/docs/)
* [Git Basics - Working with Remotes](https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes)
* [Git remotes and syncing](https://www.atlassian.com/git/tutorials/syncing)
* [GitLab Community Edition Help](https://git.kube.v1.colab.coop/help)

