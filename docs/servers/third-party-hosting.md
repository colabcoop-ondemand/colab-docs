# Third-party Hosting

Many of CoLab's clients have their own hosting on third-party commercial providers. Here are some of the most popular.

## Multiple technology capable hosting providers

These providers can host a variety of technologies and platforms.

* [Platform.sh](https://platform.sh/) - self-described as a Platform-as-a-Service (PaaS), and you deploy 70+ Open Source languages and frameworks on Platform. It has with continuous deployment functionality built in, along with multiple branch based environments support.
* [Pantheon](https://pantheon.io/) provides hosting for a number of technologies, but is most commonly used for Drupal and WordPress hosting.

## Managed Hosting Providers

These providers usually specialize in one particular framework or technology

* [WPEngine](https://wpengine.com/) is a leading managed WordPress hosting provider.
* [Flywheel](https://getflywheel.com/) ia a managed WordPress hosting provider that is aimed more at designers, with a simplified workflow and the [Local](https://localwp.com/) app for easily setting up a local dev environment, and synching between your local and the hosting environment. Flywheel was aquired by WPEngine in 2019.