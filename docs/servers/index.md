# Servers and Hosting

We see many different hosting providers and server setups in our work at CoLab. This section aims to document the most common hosting and server setups you're likely to encounter.

* [CoLab Kube](/servers/kube.md) - CoLab's own hosting solution built on Kubernetes.
* [Third party providers](/servers/third-party-hosting.md) - a breakdown of the most popular commercial hosting providers we encounter in our work at CoLab.