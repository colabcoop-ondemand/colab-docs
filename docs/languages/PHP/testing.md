# Testing PHP code

The ultimate goal of software testing is to build a quality product. We want to test our code in a way that it always returns the expected output, based on different types of input.

## Types of testing

There are many types of testing that can be used with PHP development, including:

* [Unit Testing](#unit-testing)
* [Functional Testing](#functional-testing)
* [Integration Testing](#integration-testing)
* [Acceptance Testing](#acceptance-testing)

### Unit Testing

**Unit Testing** analyzes a small piece of code is (known as a Unit). Each unit test targets a unit of code in isolation. Unit testing should be as simple as possible, and it should not depend on another functions/classes.

Unit testing in PHP is most often performed using the [PHPUnit](https://phpunit.de/) framework.

### Functional Testing

Testing based on functional requirements/specifications is called **functional testing**. Here we check given tests providing the same output as required by the end-user.

### Integration Testing

**Integration Testing** is built on top of Unit Testing. In Integration testing,  we combine two units together and check whether the combination works correctly or not. The purpose of this testing is to expose faults in the interaction between integrated units.

### Acceptance Testing

**Acceptance Testing** is the last phase of the testing process. Here we check the behavior of whole application from users side. End users insert the data and check the output whether it meets the required specifications or not. They just check the flow, not the functionality.

Acceptance testing can be performed by a non-technical person. That person can be your tester, manager or even client. If you are developing a web-application (and you probably are) the tester needs nothing more than a web browser to check that your site works correctly. You can reproduce an acceptance tester’s actions in scenarios and run them automatically using testing frameworks.

## Methodologies

### Test Driven Development

Test Driven Development (TDD) is a coding methodology where :

* Before writing any code, you think about the smallest piece of fucntionality
* Write a test for this functionality, at a high level. The test runs your function/method with specific input, and expects a certain result to be returned. On first run, the test will fail, since we haven't written any code.
* Write the simplest, quickest implementation possible of the function/method, and continue runnning the related test. Keep writing code until the test passes all the scenarios based on different inputs.
* Refactor the code you wrote, breaking it into smaller parts and make it as modular as possible.

Continually testing our code ensures that we always know exactly whether any given piece of functionality is actually working.

Tutorial: [Test-Driven Development with PHP by example](https://thephp.website/en/issue/real-life-tdd-php/)


### Behavior-Driven Development

Behavior-Driven Development (BDD) is an Agile approach that mixes requirement gathering, documentation and acceptance testing. The idea is that you start by writing human-readable sentences that describe a feature of your application and how it should work. Then you implement this behavior in software. This description can produce automated tests that will verify that the feature is implemented correctly. On the testing side, BDD tools provide you the features to perform functional or acceptance tests. There are many tools that implement the BDD concept for different languages, including PHP.

## Testing Frameworks

### PHPUnit

[PHPUnit](https://phpunit.de/) is the standard unit testing framework used for PHP.

Docs:

* [Getting Started with PHPUnit 9](https://phpunit.de/getting-started/phpunit-9.html) - explains how to install PHPUnit and get started with tetsing.
* [PHPUnit Manual](https://phpunit.readthedocs.io/en/9.5/) - complete documentation of the PHPUnit framework, version 9.5.
    * [Writing Tests for PHPUnit](https://phpunit.readthedocs.io/en/9.5/writing-tests-for-phpunit.html)

### Behat

[Behat](https://docs.behat.org/en/latest/) is an open source Behavior-Driven Development framework for PHP. It is a tool to support you in delivering software that matters through continuous communication, deliberate discovery and test-automation.

[Behat documentation](https://docs.behat.org/en/latest/guides.html)

### Codeception

[Codeception](https://codeception.com/) provides high-level domain language for tests. It is a modular system that can perform browser testing, framework testing, API testing, data-driven tests, unit and integration testing.

Docs:

* [Quickstart](https://codeception.com/quickstart)
* [Introduction](https://codeception.com/docs/01-Introduction)
* [Video tutorials](https://codeception.com/video) - a list of video tutorials for using Codeception.