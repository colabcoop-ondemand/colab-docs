# Resources

## Tutorials

* [PHP Manual](https://www.php.net/manual/en/index.php) - official PHP language documentation.
    * [Language Reference](https://www.php.net/manual/en/langref.php)
* [PHP The Right Way](https://phptherightway.com/) is an easy-to-read, quick reference for PHP popular coding standards, links to authoritative tutorials around the Web and what the contributors consider to be best practices at the present time. A living document that is continually updated, and translated into many different languages. Contributions to guide via [GitHub](https://github.com/codeguy/php-the-right-way).
* [Laracasts](https://laracasts.com/) is probably one of the best sites for learning about PHP development. Expert screencasts on Laravel, Vue, PHP and so much more. You can learn about frameworks, languages, techniques, testing, tooling and more. Like Netflix for developers. Many of the videos and even some series are free, and subscrption will unlock the full library of screencasts.
* [W3schools](https://www.w3schools.com/php/) has a section on learning PHP for beginners.