# PHP Code Documentation

It is important to document your code as clearly as possible. You may come back to a project in a few months, or even years, and you may not remember specifics of how things work. There may be many developers working on a codebase over its lifespan—proper commenting and documentation will help all devs follow the code and understand what it is supposed to do.

Good documentation may come in many forms, but generally will be in one of two forms:

* [Code comments](#code-comments)
* [Written documentation](#written-documentation)

## Code Comments

Code Commenting is the most important type of documentation, which happens within the code itself. It helps anyone reading the code know exactly what each function, class, method, or property is for, and what the logic of the app is.

Code comments are useful within a function or method, to explain step-by-step what the code is doing. Try to use inline comments wherever possible. Be as descriptive as you can, your team (and future self) will thank you.

PHP supports multiple styles of comments.

* single-line comments, starting with two forward slashes and a space (`// `)
* multiple line comments start with a forward slash and one asterisk (`/*`), and ends with an asterisk and forward slash (`*/`).
* shell-style comments starting with a hash or number sign, follwoed by a space (`# `)

```php
<?php 
echo 'This is a test'; // This is a one-line c++ style comment

/* This is a basic one line comment */

/* This is a multi line comment
yet another line of comment */
echo 'This is yet another test';

echo 'One Final Test'; # This is a one-line shell-style comment
?>
```

## PSR-5: PHPDoc

The [PSR-5:PHPDoc](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc.md) standard sets out how to document your PHP code for maximum interoperability.

### DocComment

A **DocComment** starts with a forward slash and two asterisks (`/**`), which is similar to how you start a multiline comment but with an additional asterisk, and ends with an asterisk and forward slash (`*/`). DocComments may be a single line in size but may also span multiple lines, in which case each line must start with an asterisk. It is customary, and recommended, to align the asterisks vertically when spanning multiple lines.

So, a single line DocComment looks like this:

```php
/** This is a single line DocComment. */
```

And a multiline DocComment looks like this:

```php
/**
 * This is a multi-line DocComment.
 */
```

### DocBlock

Using a **DocBlock** you are able to effectively document your application's API (Application Programming Interface) by describing the function of, and relations between, elements in your source code, such as classes and methods.

The PHPDoc definition recognizes the following _Structural Elements_:

*   [Function](https://www.php.net/language.functions)
*   [Constant](https://www.php.net/language.constants)
*   [Class](https://www.php.net/language.oop5.basic)
*   [Interface](https://www.php.net/language.oop5.interfaces)
*   [Trait](https://www.php.net/language.oop5.traits)
*   [Class constant](https://www.php.net/language.oop5.constants)
*   [Property](https://www.php.net/language.oop5.properties)
*   [Method](https://www.php.net/language.oop5.basic)

In addition to the above, the PHPDoc standard also supports DocBlocks for _Files_ and `include`/`require` statements, even though PHP itself does recognize this as a language structure.

Each of these elements must have exactly **one** DocBlock associated with it, which directly precedes it. No code or comments may be between a DocBlock and the start of an element's definition.

Commonly a DocBlock consists of the following three parts in order of appearance:

#### Summary

A short piece of text, usually one line, providing the basic function of the associated element.

#### Description

An optional longer piece of text providing more details on the associated element's function. This is very useful when working with a complex element.

#### A series of tags

These provide additional information in a structured manner. With these tags you can link to other elements, provide type information for properties and arguments, and more.

The order of these elements matter; you cannot put a summary or description after the tags as it will be seen as part of the last tag.

To follow the spec, the DocBlock must contain a `@param` tag for each parameter in your function or method, an a `@return` tag to indicate the type of result the function or method will return. There are many other optional tags. For classes or interfaces, you wil need one `@method` tag for each method.

The complete list of available tags is found in [PSR-19: PHPDoc Tags](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc-tags.md) standard.

phpDocumentor also has a useful [Tag reference](https://docs.phpdoc.org/3.0/guide/references/phpdoc/tags/index.html#tag-reference).

### PHP DocBlock example

```php
<?php
/**
 * A summary informing the user what the associated element does.
 *
 * A *description*, that can span multiple lines, to go _in-depth_ into
 * the details of this element and to provide some background information
 * or textual references.
 *
 * @param string $myArgument With a *description* of this argument,
 *                           these may also span multiple lines.
 *
 * @return void
 */
 function myFunction($myArgument)
 {
 }
```

### Documentation generators

Tools such as [phpDocumentor](https://phpdoc.org/3.0/) can auto-generate documentation sites based on code comments following the PHPDoc Standard.

