# Programming Paradigms

PHP is a flexible, dynamic language that supports a variety of programming techniques. 

## Procedural Programming

Procedural programming is based upon the concept of the procedure call. Procedures, also known as routines, subroutines, or functions, simply contain a series of computational steps to be carried out.

Think of it as writing a list of instructions to tell the computer what to do step by step. It relies on procedures or routines.

## Object-oriented Programming

Object-oriented programming (OOP) is a programming paradigm based on the concept of “objects”, which may contain data, in the form of fields, often known as attributes; and code, in the form of procedures, often known as methods.

PHP has a very complete set of object-oriented programming features including support for classes, abstract classes, interfaces, inheritance, constructors, cloning, exceptions, and more.

*   [Read about Object-oriented PHP](https://secure.php.net/language.oop5)
*   [Read about Traits](https://secure.php.net/language.oop5.traits)

## Functional Programming

Functional programming is a style of building the structure and elements of computer programs — that treats computation as the evaluation of mathematical functions and avoids changing-state and mutable data.

PHP supports first-class functions, meaning that a function can be assigned to a variable. Both user-defined and built-in functions can be referenced by a variable and invoked dynamically. Functions can be passed as arguments to other functions (a feature called _Higher-order Functions_) and functions can return other functions.

Read more: [Functional Programming in PHP](/pages/Functional-Programming.html)

## Meta Programming

PHP supports various forms of meta-programming through mechanisms like the Reflection API and Magic Methods. There are many Magic Methods available like `__get()`, `__set()`, `__clone()`, `__toString()`, `__invoke()`, etc. that allow developers to hook into class behavior. Ruby developers often say that PHP is lacking `method_missing`, but it is available as `__call()` and `__callStatic()`.

*   [Read about Magic Methods](https://secure.php.net/language.oop5.magic)
*   [Read about Reflection](https://secure.php.net/intro.reflection)
*   [Read about Overloading](https://secure.php.net/language.oop5.overloading)

