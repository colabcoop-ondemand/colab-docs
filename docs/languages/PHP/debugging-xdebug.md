# Debugging with Xdebug

One of the most useful tools in software development is a proper debugger. It allows you to trace the execution of your code and monitor the contents of the stack. [Xdebug](https://xdebug.org/), PHP’s debugger, can be utilized by various IDEs to provide Breakpoints and stack inspection. It can also allow tools to perform code coverage analysis and code profiling.

If you find yourself in a bind, willing to resort to `var_dump()`/`print_r()`, and you still can’t find the solution - maybe you need to use the debugger.

## Install Xdebug

[Install Xdebug](https://xdebug.org/docs/install)

Graphical debuggers make it very easy to step through code, inspect variables, and eval code against the live runtime. Many IDE’s have built-in or plugin-based support for graphical debugging with Xdebug.

*   [Learn more about Xdebug](https://xdebug.org/docs/)
