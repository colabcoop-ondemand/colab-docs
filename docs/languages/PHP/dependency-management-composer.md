# Dependency Management

It is very useful to use existing packages for specific functionality, instead of always writing code from scratch. It lowers the burden of maintaining code for your project.

PHP has robust tools for dependency management to efficiently manage these external packages.

## Composer

[Composer](https://getcomposer.org/) is the standard tool for dependency management in PHP. It allows you to declare the libraries your project depends on and it will manage (install/update) them for you. It is similar in function to [npm](https://www.npmjs.com/) in JavaScript, or ruby's [bundler](https://bundler.io/).

There are a plethora of PHP libraries that are compatible with Composer and ready to be used in your project. These “packages” are listed on [Packagist](https://packagist.org/), the official repository for Composer-compatible PHP libraries.

### How to Install Composer

Composer can be installed as a local dependency on each project, or globally as a system wide executable.

[Composer installation docs](https://getcomposer.org/doc/00-intro.md#installation-linux-unix-macos)


### How to Define and Install Dependencies

Composer keeps track of your project’s dependencies in a file called `composer.json`. You can manage it by hand if you like, or use Composer itself. The `composer require` command adds a project dependency and if you don’t have a `composer.json` file, one will be created. 

The `composer.json` file should typically go in the top-most directory of your project/VCS repository. 

[Learn more about the makeup of the composer.json file](https://getcomposer.org/doc/01-basic-usage.md#composer-json-project-setup).

To declare dependencies, we will refer to each depency by its package name, which consists of a vendor name and the project's name. The *vendor name* generally refers to either an overaching parent project, or the organization that created the package. The *project name* is the name of the specific project within that vendor name space. All Composer packages _must_ be vendor-namespaced. This means Composer only allows packages such as `"symfony/finder"` and `"nette/finder"`, but _never_ stand-alone package names such as `"finder"`. 

Here’s an example that adds [Twig](https://twig.symfony.com/) as a dependency of your project.

    composer require twig/twig

You can also specify a specific version of a package, a minimum version, or any range of versions, using [version constraints](https://getcomposer.org/doc/articles/versions.md).

    composer require twig/twig:^2.0

The preceeding command specifies a *minimum* version of 2.0, and any version in the 2.x series, but not 3.0 or above. Composer packages follow [semantic versioning](https://semver.org/).

Alternatively, the `composer init` command will guide you through creating a full `composer.json` file for your project. Either way, once you’ve created your `composer.json` file you can tell Composer to download and install your dependencies into the `vendor/` directory. This also applies to projects you’ve downloaded that already provide a `composer.json` file:

    composer install

Next, add this line to your application’s primary PHP file; this will tell PHP to use Composer’s autoloader for your project dependencies.

```php
<?php
require 'vendor/autoload.php';
```

Now you can use your project dependencies, and they’ll be autoloaded on demand.

### Updating your dependencies

Composer creates a file called `composer.lock` which stores the exact version of each package it downloaded when you first ran `composer install`. If you share your project with others, ensure the `composer.lock` file is included, so that when they run `composer install` they’ll get the same versions as you. To update your dependencies, run `composer update`. Don’t use `composer update` when deploying, only `composer install`, otherwise you may end up with different package versions on production.

This is most useful when you define your version requirements flexibly. For instance, a version requirement of `~1.8` means “anything newer than `1.8.0`, but less than `2.0.x-dev`”. You can also use the `*` wildcard as in `1.8.*`. Now Composer’s `composer update` command will upgrade all your dependencies to the newest version that fits the restrictions you define.

### Checking your dependencies for security issues

The [Local PHP Security Checker](https://github.com/fabpot/local-php-security-checker) is a command-line tool, which will examine your `composer.lock` file and tell you if you need to update any of your dependencies.

### Block packages with known vulnerabilities

[`roave/security-advisories`](https://github.com/Roave/SecurityAdvisories) is a composer meta-package that has no functional code by itself, but has a `composer.json` file that prevents packages and package versions with known vulnerabilities from being installed.

    composer require --dev roave/security-advisories:dev-latest

Running above, or adding `roave/security-advisories` to `require-dev` section in the `composer.json` file in a project does the trick. Once installed, any packages with known vulnerabilities will be considered a _conflict_ package version, and Composer will refuse to install the vulnerable package or the vulnerable version of one.

A [Drupal-specific advisories list](https://github.com/drupal-composer/drupal-security-advisories) (that can be used in addition to `roave/security-advisories`) and a [WordPress list](https://github.com/PHPWatch/WordPress-Security-Advisories) are also available.


### Handling global dependencies with Composer

Composer can also handle global dependencies and their binaries. Usage is straight-forward, all you need to do is prefix your command with `global`. If for example you wanted to install PHPUnit and have it available globally, you’d run the following command:

    composer global require phpunit/phpunit

This will create a `~/.composer` folder where your global dependencies reside. To have the installed packages’ binaries available everywhere, you’d then add the `~/.composer/vendor/bin` folder to your `$PATH` variable.

## Packagist

[Packagist.org](https://packagist.org/) is the main Composer repository. A Composer repository is basically a package source: a place where you can get packages from. Packagist aims to be the central repository that everybody uses. This means that you can automatically `require` any package that is available there, without further specifying where Composer should look for the package.

If you go to the [Packagist.org website](https://packagist.org/), you can browse and search for packages.

Any open source project using Composer is recommended to publish their packages on Packagist. A library does not need to be on Packagist to be used by Composer, but it enables discovery and adoption by other developers more quickly.

The Packagist repository is automatically configured as a source repository for Composer.

## Learn more about Composer

To learn more about how Composer works, visit the official [Composer documentation](https://getcomposer.org/doc/) site.
