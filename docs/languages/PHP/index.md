# PHP

**PHP** is a general-purpose scripting language geared towards web development.

PHP is the scripting component of the most common server stack - LAMP (Linux, Apache, MySQL, PHP).

As of March 2021, PHP was used as the server-side programming language on 79.1% of websites. PHP is used for popular open source web content management systems including WordPress, Drupal, and Joomla.

## Section Index

* [PHP Coding Standards](./standards-psr.md)
* [Programming Paradigms](./programming-paradigms.md)
* [Dependency Management with Composer](./dependency-management-composer.md)
* [Code Documentation](./documentation.md)
* [Testing](./testing.md) - testing frameworks for PHP programming
* [Debugging with Xdebug](./debugging-xdebug.md)
* [IDEs and code editors](./IDEs-code-editors.md)
* [Resources](./resources.md) - further resources and tutorials on PHP development.
