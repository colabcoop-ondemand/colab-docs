# IDEs and Code Editors

## PhpStorm
[PhpStorm](https://www.jetbrains.com/phpstorm/) - A full Integrated Development Environment (IDE) geared towards PHP development. Commercial, US $199.00 / year or US $19.90 / month.

* [PhpStorm - Getting Started](https://www.jetbrains.com/phpstorm/documentation/) - official documentation from JetBrains, developers of PhpStorm.
* [How Jeffrey Would Setup PHPStorm](https://laracasts.com/series/jeffreys-larabits/episodes/1) - video tutorial from Jeffrey Way of Laracasts.

## VS Code

[Visual Studio Code](https://code.visualstudio.com/) (VS Code) - A popular open-source code editor, with built-in PHP support, like syntax highlighting and bracket matching, IntelliSense (code completion), and you can add more functionality through community-created [extensions](https://code.visualstudio.com/docs/editor/extension-marketplace).

* [PHP in Visual Studio Code](https://code.visualstudio.com/docs/languages/php).
* [Transform VS Code into a Formidable PHP Experience](https://laracasts.com/series/andrews-larabits/episodes/1) - video tutorial from Andrew Schmelyun of Laracasts.

## Vim

[Vim](https://www.vim.org/) (or Vi IMproved) is one of the oldest and most popular open source text editors. CLI based, included (and/or pre-installed) with most Linux and Unix-like systems including macOS. Extremely powerful, but with a steep learning curve.

* [Vim Mastery](https://laracasts.com/series/vim-mastery) - video tutorial series from Laracasts.

Other popular editors include: [Atom](https://atom.io/), [Brackets](http://brackets.io/), [Bluefish](https://bluefish.openoffice.nl/index.html), [Notepad++](https://notepad-plus-plus.org/), [TextMate](https://macromates.com/)

## Sublime Text

[Sublime Text](https://www.sublimetext.com/) - Commercial $99 USD for 3 years of updates. A popular text editor with support for numerous languages including PHP.

* [Sublime Text Mastery](https://laracasts.com/series/sublime-text-mastery) - video tutorial series from Laracasts.
