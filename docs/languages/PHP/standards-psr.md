# PHP Coding Standards

## PHP Standards Recommendations (PSRs)

The primary coding standards for the PHP community are set by the PHP Framework Interop Group (PHP-FIG).

The main set of standards for PHP development are the PHP Standards Recommendations (PSRs). 

[Complete list of PSRs](https://www.php-fig.org/psr/) 

All PHP development should aim to follow the PSRs as closely as possible.

The PSRs that apply most to day-to-day development work at CoLab are:

* [PSR-1: Basic Coding Standard](https://www.php-fig.org/psr/psr-1/) - standard coding elements that are required to ensure a high level of technical interoperability between shared PHP code.
* [PSR-12: Extended Coding Style](https://www.php-fig.org/psr/psr-12/) - extends the basic standards, and enumerates a shared set of rules and expectations about how to format PHP code.
* [PSR-4: Autoloader](https://www.php-fig.org/psr/psr-4/) - standard for autoloading of classes from file paths, and namespacing for classes.
* [PSR-5: PHPDoc](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc.md) - standard for code documentation using inline comments.
* [PSR-19: PHPDoc tags](https://github.com/php-fig/fig-standards/blob/master/proposed/phpdoc-tags.md) - list of the individual tags used for PHPDoc formatted comments.S

## Implementing coding standards

To help implement the PSRs in your development, there are various tools you can use.

[PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer) (PHPCS) is a set of two PHP scripts; the main `phpcs` script that tokenizes PHP, JavaScript and CSS files to detect violations of a defined coding standard, and a second `phpcbf` script to automatically correct coding standard violations. PHP_CodeSniffer is an essential development tool that ensures your code remains clean and consistent.

If using Composer for dependecy management (you really should), then you can add PHPCS to your dev dependencies:

```bash
composer require --dev squizlabs/php_codesniffer
```

PHPCS can be used via the CLI, or integrated with code editors, IDEs or build tooling. You can also set up pre-comit hooks in git to enforce coding standards.

## Resources
* Article: [Set Up PHP CodeSniffer for Local Development](https://www.twilio.com/blog/set-up-php-codesniffer-local-development-sublime-text-php-storm-vs-code)
* Guide: [How to set up commit hooks for PHP](https://dev.to/bdelespierre/how-to-setup-git-commit-hooks-for-php-42d1)
* Tutorial: [What is a DocBlock?](https://docs.phpdoc.org/guide/guides/docblocks.html) - phpDocumentor documentation.
