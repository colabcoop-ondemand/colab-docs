# Cascading Style Sheets (CSS)

CSS (Cascading Style Sheets) is the language for describing the presentation of Web pages, including colors, layout, and fonts. It allows one to adapt the presentation to different types of devices, such as large screens, small screens, or printers. CSS is independent of HTML and can be used with any XML-based markup language. The separation of HTML from CSS makes it easier to maintain sites, share style sheets across pages, and tailor pages to different environments. This is referred to as the separation of structure (or: content) from presentation.

## Resources

* [W3C - HTML & CSS](HTML & CSS)
* [MDN CSS Docs](https://developer.mozilla.org/en-US/docs/Web/CSS) - Mozilla Developer Network
* CSS Tricks:
    - [A Complete Guide to Flexbox](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
    - [A Complete Guide to CSS Grid](https://css-tricks.com/snippets/css/complete-guide-grid/)
* [Laying Out A Flexible Future For Web Design With Flexbox Best Practices](https://www.smashingmagazine.com/2015/08/flexible-future-for-web-design-with-flexbox/) - Smashing Magazine

## Guidelines

### Formatting
---------------
CSS declarations should be formatted like so:
@TODO

### Responsive
---------------
All delivered CSS should be tested at all widths down to at least 320px and preferably 240px. Supporting the smallest screens can be seen as a form of serving content to those who can't afford high-resolutions screens.

### Fonts
---------------
Root font size should be set to the html element in px, preferably 16px for desktop, and if needed, media query adjustments can be made to that base size to scale up/down for readability in mobile, using rems to handle scaling for font sizes that should be relative to the base size. px should not be used to set font sizes except where it is needed to override the root font size in limited use-cases.

### Preferred units of measure
---------------
Prefer %s to other measures.