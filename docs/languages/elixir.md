# Elixir

* [Official homepage](https://elixir-lang.org/)
* [Documentation](https://elixir-lang.org/docs.html)

Elixir is a dynamic, functional language for building scalable and maintainable applications.
Elixir leverages the Erlang VM, known for running low-latency, distributed, and fault-tolerant systems. Elixir is successfully used in web development, embedded software, data ingestion, and multimedia processing, across a wide range of industries.

	