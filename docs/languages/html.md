# HTML

Hypertext Markup Language, or HTML, is the language for describing the structure of Web pages. HTML gives authors the means to:

* Publish online documents with headings, text, tables, lists, photos, etc.
* Retrieve online information via hypertext links, at the click of a button.
* Design forms for conducting transactions with remote services, for use in searching for information, making reservations, ordering products, etc.
* Include spread-sheets, video clips, sound clips, and other applications directly in their documents.

With HTML, authors describe the structure of pages using markup. The elements of the language label pieces of content such as “paragraph,” “list,” “table,” and so on.

* [W3C - HTML & CSS](https://www.w3.org/standards/webdesign/htmlcss)
* [HTML official specification](https://html.spec.whatwg.org/multipage/)
* [HTML - Mozilla Dev Network](https://developer.mozilla.org/en-US/docs/Web/HTML)