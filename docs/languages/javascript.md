# JavaScript / ECMAScript

JavaScript is a high-level, often just-in-time compiled language that conforms to the ECMAScript standard. Note this is a separate language from the Java platform.

It has dynamic typing, prototype-based object-orientation, and first-class functions. It is multi-paradigm, supporting event-driven, functional, and imperative programming styles. It has application programming interfaces (APIs) for working with text, dates, regular expressions, standard data structures, and the Document Object Model (DOM).

## Documentation

* [ECMA 2021 Standards](https://www.ecma-international.org/publications-and-standards/standards/ecma-262/)