# CoLab Developer Documentation

The purpose of this documentation guide is to foster community learning by laying down a collaborative framework for development guidelines.

Our goal is to inspire a culture of continual learning through knowledge sharing. We seek to create a space where we continually mentor and encourage one another to become better developers while respecting each others' knowledge and expertise.

## Contributing

This is meant to be a living document, and we aim to keep it up to date with the latest techologies and development practises. All are welcome and encouraged to contribute.

We all participate in creating these guidelines. If you want to recommend new guidelines, or modify existing guidelines, please make a branch for your contributions, and create a pull request. Most of the time you'll probably see it merged in.

### Writing Documentation

This guide is written using [Markdown][md], a lightweight markup language which results in easy-to-read, easy-to-write plain text documents that can be converted to valid HTML documents in a predictable manner.

* Learn [Markdown syntax][syntax]
* For full documentation on MkDocs visit [mkdocs.org](https://www.mkdocs.org).

## Building this guide

To build this guide locally, first [install MkDocs](https://www.mkdocs.org/getting-started/). Then you can run the following commands:

* `mkdocs serve` - Start the live-reloading docs server. This is useful for previewing in a browser while you write documentation in your code editor.
* `mkdocs build` - Build the documentation site. It will be compiled into HTML within the 'site' directory.

## Guide layout

    mkdocs.yml    # The mkdocs configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

[md]: https://daringfireball.net/projects/markdown/
[syntax]: https://daringfireball.net/projects/markdown/syntax
