# CoLab Development Practices and Standards

The purpose of this documentation guide is to foster community learning by laying down a collaborative framework for development guidelines.

Our goal is to inspire a culture of continual learning through knowledge sharing. We seek to create a space where we continually mentor and encourage one another to become better developers while respecting each others' knowledge and expertise.

## Building this guide

To build this guide locally, first [install MkDocs](https://www.mkdocs.org/getting-started/). Then you can run the following commands:

* `mkdocs serve` - Start the live-reloading docs server. This is useful for previewing in a browser while you write documentation in your code editor.
* `mkdocs build` - Build the documentation site. It will be compiled into HTML within the 'site' directory.

## Guide layout

    mkdocs.yml    # The mkdocs configuration file.
    docs/
        index.md  # The documentation homepage.
        ...       # Other markdown pages, images and other files.

[md]: https://daringfireball.net/projects/markdown/
[syntax]: https://daringfireball.net/projects/markdown/syntax
